#ifndef _E2010CMD_H
#define _E2010CMD_H

#define V_RESET_FPGA_E2010            0
#define V_PUT_ARRAY_E2010             1
#define V_GET_ARRAY_E2010             2
#define V_INIT_FPGA_E2010             3
#define V_START_ADC_E2010             4
#define V_STOP_ADC_E2010              5
#define V_GET_MODULE_NAME_E2010       11
#define V_CALL_APPLICATION_E2010      15
// 13 - special put_array


// synchro config
#define INT_START_TRANS       0x01
#define INT_START             0x81
#define EXT_START_UP          0x84
#define EXT_START_DOWN        0x94
#define EXT_START_DOWN_REVB   0x8C // (0x84+0x08)

// clock src
#define INT_CLK_TRANS         0x00
#define INT_CLK               0x40
#define EXT_CLK_UP            0x42
#define EXT_CLK_DOWN          0x62

//bit macros for channel input range and mode config, use |/+ operator to configure
#define V30_0              0x0000
#define V10_0              0x0008
#define V03_0              0x0010
#define GND_0              0x0000
#define SIG_0              0x0400

#define V30_1              0x0000
#define V10_1              0x0002
#define V03_1              0x0004
#define GND_1              0x0000
#define SIG_1              0x0200

#define V30_2              0x0000
#define V10_2              0x8000
#define V03_2              0x0100
#define GND_2              0x0000
#define SIG_2              0x1000

#define V30_3              0x0000
#define V10_3              0x2000
#define V03_3              0x4000
#define GND_3              0x0000
#define SIG_3              0x0800

// advanced analog sync
#define A_SYNC_OFF         0x00000000
#define A_SYNC_UP_EDGE     0x00000080
#define A_SYNC_DOWN_EDGE   0x00000084
#define A_SYNC_HL_LEVEL    0x00000088
#define A_SYNC_LH_LEVEL    0x0000008C

// advanced translation mode for syn start pins
#define SYNC_TRANS_START_SREG     0x01000000
#define SYNC_TRANS_START_SREG_INV 0x02000000
#define START_TRANS_SYNC_SREG     0x00010000
#define START_TRANS_SYNC_SREG_INV 0x00020000


// channel num macros
#define CH_0               0x00
#define CH_1               0x01
#define CH_2               0x02
#define CH_3               0x03


#define SEL_TEST_MODE           (0x81000000UL)
#define SEL_AVR_DM              (0x82000000UL)
#define SEL_AVR_PM              (0x83000000UL)
#define SEL_DIO_PARAM           (0x84000000UL)
#define SEL_DIO_DATA            (0x85000000UL)
#define SEL_ADC_PARAM           (0x86000000UL)
#define SEL_EXT_ADC_PARAM       (0x87000000UL)
#define SEL_BULK_REQ_SIZE       (0x88000000UL)
#define SEL_DAC_DATA            (0x89000000UL)
#define SEL_ADC_CALIBR_KOEFS    (0x8A000000UL)
#define SEL_PLD_DATA            (0x8B000000UL) // данные для заливки в ПЛИС
#define SEL_MODULE_TG_DATA      (0x8C000000UL) // признак Temperature Grade у железа модуля (с версии прошивки ПЛИС 02.0x.12)
#define SEL_PLD_TG_DATA         (0x8D000000UL) // признак Temperature Grade у прошивки ПЛИС'а (с версии прошивки ПЛИС 02.0x.12)
#define SEL_DIG_RATE            (0x8E000000UL) // делитель частоты цифрового ввода данных (с версии прошивки ПЛИС 02.0x.13)
#define SEL_PLD_INFO            (0x8F000000UL) // чтение строки PLD Info (для Rev.'C')
#define SEL_DESCRIPTOR          (0x91000000UL) // новый запрос для работы с дескрипторами (для модуля Rev.'C' и выше)
#define SEL_NV_FLAGS            (0x92000000UL) // дополнительные возможности модуля: управление индикацией светодиода и т.д. (с версии прошивки AVR 2.6 и выше (Rev.'B') или 3.2 и выше (Rev.'C')

#define FIRMWARE_START_ADDRESS            (SEL_AVR_PM | 0x0000L)
#define USER_FLASH_ADDRESS                (SEL_AVR_PM | 0x2D00L)
#define FIRMWARE_DESCRIPTOR_ADDRESS       (SEL_AVR_PM | 0x2F00L)
#define MODULE_DESCRIPTOR_ADDRESS         (SEL_AVR_PM | 0x3000L)
#define BOOT_LOADER_START_ADDRESS         (SEL_AVR_PM | 0x3C00L)
#define BOOT_LOADER_DESCRIPTOR_ADDRESS    (SEL_AVR_PM | 0x3FB0L)

#define NEW_USER_FLASH_ADDRESS              (SEL_DESCRIPTOR	| 0x1D00UL)
#define NEW_FIRMWARE_DESCRIPTOR_ADDRESS	    (SEL_DESCRIPTOR	| 0x1F00UL)
#define NEW_MODULE_DESCRIPTOR_ADDRESS       (SEL_DESCRIPTOR	| 0x3000UL)


#define DATA_STATE_ADDRESS                (SEL_AVR_DM | (0x0150L + 0x00L))
#define EXTRA_SYNCHRO_PARS_ADDRESS        (SEL_AVR_DM | (0x0150L + 13))
#define ADC_CORRECTION_ADDRESS            (SEL_AVR_DM | (0x0150L + 13 + 13))
#define LUSBAPI_OR_LCOMP_ADDRESS          (SEL_AVR_DM | (0x0150L + 13 + 13 + 1))
#define EXT_SYNC_REG_ADDRESS              (SEL_AVR_DM | (0x0150L + 13 + 13 + 2))
#define EXT_START_REG_ADDRESS             (SEL_AVR_DM | (0x0150L + 13 + 13 + 3))

#define PLD_INFO_ADDRESS                  (SEL_PLD_INFO)
#define NV_FLAGS_REG_ADDRESS              (SEL_NV_FLAGS | 0x0000L)



#define EP1K10_SIZE      (22*1024+100)

#endif
